package com.soaexpert.sns.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.codahale.metrics.annotation.Metered;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Path("/sns")
@Consumes("*/*")
@Produces("text/plain")
public class TesteSNSResource extends BaseSNSResource {
    @Override
    @Metered
    public void handleNotification(String endpointId, ObjectNode bodyNode) {
        super.handleNotification(endpointId, bodyNode);

        logger.info("Nova mensagem para o endpoint {}: {}", endpointId, bodyNode);
    }

}